if (!place_meeting(x,y+1,obj_block)) {
	yMove+=.5;	
}

if (yMove > 6) {
	yMove = 6;	
}

//Collision Check
if (place_meeting(x+xMove,y,obj_block))
{
    while(!place_meeting(x+sign(xMove),y,obj_block))
    {
        x += sign(xMove);
    }
    xMove = 0;
}

if (place_meeting(x,y+yMove,obj_block))
{
    while(!place_meeting(x,y+sign(yMove),obj_block))
    {
        y += sign(yMove);
    }
    yMove = 0;
	xMove = 0;
}

x += xMove;
y += yMove;