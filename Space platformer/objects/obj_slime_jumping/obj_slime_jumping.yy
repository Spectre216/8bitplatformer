{
    "id": "51773c9f-e5aa-4976-8fe6-e5faeafc1910",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_slime_jumping",
    "eventList": [
        {
            "id": "8f776652-5f9e-4d93-9a61-80f494049805",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51773c9f-e5aa-4976-8fe6-e5faeafc1910"
        },
        {
            "id": "abf88016-c16e-4495-81a3-079dedf6d2e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "51773c9f-e5aa-4976-8fe6-e5faeafc1910"
        },
        {
            "id": "c2b9832a-bac0-4659-801b-d91b7702d2a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "51773c9f-e5aa-4976-8fe6-e5faeafc1910"
        },
        {
            "id": "fba14495-74d5-4eb3-a8cb-f008e7b1333c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "51773c9f-e5aa-4976-8fe6-e5faeafc1910"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dd24a80b-0468-49c3-b516-47c8dea9a216",
    "visible": true
}