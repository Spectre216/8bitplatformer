//Player movement
var move = (keyboard_check(ord("S"))) - (keyboard_check(ord("A")))

if (move != 0 && !isJumping) {
	image_xscale = move;
	sprite_index = spr_pc_walk;
} else if (move == 0 && !isJumping) {
	sprite_index = spr_pc_stand;	
} else if (isJumping) {
	sprite_index = spr_pc_jump;	
}

if (keyboard_check_pressed(vk_space) && !isJumping) {
	isJumping = true;
	yMove -= ySpeed;
	
}

if (!place_meeting(x,y+1,obj_block)) {
	yMove+=.5;	
}

if (yMove > 6) {
	yMove = 6;	
}

xMove = move * xSpeed;

//Collision Check
if (place_meeting(x+xMove,y,obj_block))
{
    while(!place_meeting(x+sign(xMove),y,obj_block))
    {
        x += sign(xMove);
    }
    xMove = 0;
}

if (place_meeting(x,y+yMove,obj_block))
{
    while(!place_meeting(x,y+sign(yMove),obj_block))
    {
        y += sign(yMove);
    }
    yMove = 0;
	isJumping = false;
}

x += xMove;
y += yMove;