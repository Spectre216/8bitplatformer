//Movement
var move = xSpeed * facing;

if (!place_meeting(x,y+1,obj_block)) {
	yMove+=.5;	
}

if (yMove > 6) {
	yMove = 6;	
}

//Collision Check
if (place_meeting(x+move,y,obj_block))
{
    while(!place_meeting(x+sign(move),y,obj_block))
    {
        x += sign(move);
    }
    move = 0;
}

if (place_meeting(x,y+yMove,obj_block))
{
    while(!place_meeting(x,y+sign(yMove),obj_block))
    {
        y += sign(yMove);
    }
    yMove = 0;
}

//Adjust X
x += move
y+= yMove;