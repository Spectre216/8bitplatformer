{
    "id": "3d5db576-a362-4b7d-b688-807eee7aa979",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_slime_moving",
    "eventList": [
        {
            "id": "393e3e99-efbe-4b8b-8956-c19a2d821815",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d5db576-a362-4b7d-b688-807eee7aa979"
        },
        {
            "id": "38ab443c-60fe-4716-94c7-70c6333ae852",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3d5db576-a362-4b7d-b688-807eee7aa979"
        },
        {
            "id": "91f48360-b679-4786-9c77-7fb9999e1f80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3d5db576-a362-4b7d-b688-807eee7aa979"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "241c1a7c-7aa0-4da7-b970-0b6b17cac3c2",
    "visible": true
}