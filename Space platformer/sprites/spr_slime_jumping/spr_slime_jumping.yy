{
    "id": "58a458bc-5c7e-4232-81d1-b09df5af0b77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slime_jumping",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 2,
    "bbox_right": 5,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "147bc345-c3d6-452a-a4cc-967b1b709ccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a458bc-5c7e-4232-81d1-b09df5af0b77",
            "compositeImage": {
                "id": "7ff4d80b-6041-4cf5-8b23-e88218ad8f60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "147bc345-c3d6-452a-a4cc-967b1b709ccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd5e7b85-e531-4b6e-b161-60a57db8f6e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "147bc345-c3d6-452a-a4cc-967b1b709ccd",
                    "LayerId": "9dde8735-ca4f-427f-9360-50baebf47bd5"
                }
            ]
        },
        {
            "id": "195abad2-2355-471f-b830-eaa61519b202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a458bc-5c7e-4232-81d1-b09df5af0b77",
            "compositeImage": {
                "id": "2ba637a9-38b1-46c2-a1b5-0051d42b292e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "195abad2-2355-471f-b830-eaa61519b202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "412d9483-0a7a-40c5-aba6-08673ee57a69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "195abad2-2355-471f-b830-eaa61519b202",
                    "LayerId": "9dde8735-ca4f-427f-9360-50baebf47bd5"
                }
            ]
        },
        {
            "id": "8f0e1fd9-3a87-4c98-a0a4-275b77653280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a458bc-5c7e-4232-81d1-b09df5af0b77",
            "compositeImage": {
                "id": "cdf7ddbf-ea31-475e-a714-fe6c7419116f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f0e1fd9-3a87-4c98-a0a4-275b77653280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4b41cc0-0277-4395-847d-24d10e11e67f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f0e1fd9-3a87-4c98-a0a4-275b77653280",
                    "LayerId": "9dde8735-ca4f-427f-9360-50baebf47bd5"
                }
            ]
        },
        {
            "id": "eebe7920-7c86-47bf-adef-e3e439a2b840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a458bc-5c7e-4232-81d1-b09df5af0b77",
            "compositeImage": {
                "id": "a2c52e69-7bd8-43d2-b427-14cf46975946",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eebe7920-7c86-47bf-adef-e3e439a2b840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f9330d3-4384-47e5-94ff-ea92a7ead3ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eebe7920-7c86-47bf-adef-e3e439a2b840",
                    "LayerId": "9dde8735-ca4f-427f-9360-50baebf47bd5"
                }
            ]
        },
        {
            "id": "3d4a58cb-ca48-4f3f-a9f9-7643b966c340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a458bc-5c7e-4232-81d1-b09df5af0b77",
            "compositeImage": {
                "id": "37e71f70-39f8-465d-9f92-17cc0276f114",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d4a58cb-ca48-4f3f-a9f9-7643b966c340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ced1010e-7405-4e07-80c1-62cda7fe4973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d4a58cb-ca48-4f3f-a9f9-7643b966c340",
                    "LayerId": "9dde8735-ca4f-427f-9360-50baebf47bd5"
                }
            ]
        },
        {
            "id": "c1b1a601-0ee3-4f07-9ccd-405ab85b49e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a458bc-5c7e-4232-81d1-b09df5af0b77",
            "compositeImage": {
                "id": "54bbe538-13af-43b6-80b3-ba4dc58e1b87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1b1a601-0ee3-4f07-9ccd-405ab85b49e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ddfdcdb-a439-427a-83ec-2adb8d3d696d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1b1a601-0ee3-4f07-9ccd-405ab85b49e1",
                    "LayerId": "9dde8735-ca4f-427f-9360-50baebf47bd5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "9dde8735-ca4f-427f-9360-50baebf47bd5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58a458bc-5c7e-4232-81d1-b09df5af0b77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}