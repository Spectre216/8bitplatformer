{
    "id": "77125f9a-2b55-461e-a1b1-c183ce910c89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pc_stand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 2,
    "bbox_right": 5,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10935357-461b-46f7-acf9-a32edf7759e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77125f9a-2b55-461e-a1b1-c183ce910c89",
            "compositeImage": {
                "id": "f0d405fb-e93c-413a-a2d9-732c0e4d3ff6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10935357-461b-46f7-acf9-a32edf7759e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "889d095c-fea3-4e38-a9b6-13eed8f3f3fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10935357-461b-46f7-acf9-a32edf7759e4",
                    "LayerId": "8d5fd2a1-b15d-40b2-9800-88b7f2b3acca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "8d5fd2a1-b15d-40b2-9800-88b7f2b3acca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77125f9a-2b55-461e-a1b1-c183ce910c89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}