{
    "id": "dd24a80b-0468-49c3-b516-47c8dea9a216",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slime_still",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89150817-40dd-4a1e-b12a-3213ef396ac0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd24a80b-0468-49c3-b516-47c8dea9a216",
            "compositeImage": {
                "id": "b91f5476-7662-4b65-81d5-252df352c943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89150817-40dd-4a1e-b12a-3213ef396ac0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0a7520f-b3ca-442f-8240-f7877b39544c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89150817-40dd-4a1e-b12a-3213ef396ac0",
                    "LayerId": "d898f5d5-d430-49e7-8351-92823fd6aafc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "d898f5d5-d430-49e7-8351-92823fd6aafc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd24a80b-0468-49c3-b516-47c8dea9a216",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}