{
    "id": "972a3335-4b46-4fcc-9bdf-044ca9b45534",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pc_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 2,
    "bbox_right": 5,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58401a3a-e59e-4b38-997c-2bbbe97bbbeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972a3335-4b46-4fcc-9bdf-044ca9b45534",
            "compositeImage": {
                "id": "e428b871-cd97-406f-9302-3f087bce97b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58401a3a-e59e-4b38-997c-2bbbe97bbbeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c851e4c5-d22c-4c74-947d-5439d82ef22b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58401a3a-e59e-4b38-997c-2bbbe97bbbeb",
                    "LayerId": "41ad21e8-09b2-471a-9b54-61717aa3c6dc"
                }
            ]
        },
        {
            "id": "fde09407-1ae1-41b4-8e8c-bb6f5253367d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972a3335-4b46-4fcc-9bdf-044ca9b45534",
            "compositeImage": {
                "id": "9357abd7-569c-48ae-bbd3-c2f5c5814cd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde09407-1ae1-41b4-8e8c-bb6f5253367d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f7d79e3-ea99-44cd-a948-876a79c1dda0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde09407-1ae1-41b4-8e8c-bb6f5253367d",
                    "LayerId": "41ad21e8-09b2-471a-9b54-61717aa3c6dc"
                }
            ]
        },
        {
            "id": "f591b20c-1d58-4555-be14-a86cc09d51e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972a3335-4b46-4fcc-9bdf-044ca9b45534",
            "compositeImage": {
                "id": "0324153a-06c4-488c-b5c9-32585f890d9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f591b20c-1d58-4555-be14-a86cc09d51e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea6d4f5c-845b-4b70-afe8-05bdb20a070a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f591b20c-1d58-4555-be14-a86cc09d51e3",
                    "LayerId": "41ad21e8-09b2-471a-9b54-61717aa3c6dc"
                }
            ]
        },
        {
            "id": "84e02484-354b-4a55-b8f7-c36f83b92e25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972a3335-4b46-4fcc-9bdf-044ca9b45534",
            "compositeImage": {
                "id": "2261cab8-0082-4eb9-89a7-ee0af68810ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84e02484-354b-4a55-b8f7-c36f83b92e25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c593c1d-239d-4396-876b-6878ecc77c67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84e02484-354b-4a55-b8f7-c36f83b92e25",
                    "LayerId": "41ad21e8-09b2-471a-9b54-61717aa3c6dc"
                }
            ]
        },
        {
            "id": "29f78148-53e4-4229-aa47-21538ec77c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972a3335-4b46-4fcc-9bdf-044ca9b45534",
            "compositeImage": {
                "id": "2e97f139-6196-4691-87a5-2532dbe495b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29f78148-53e4-4229-aa47-21538ec77c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d3787c8-80c1-449b-aaf7-2cf37d6632a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29f78148-53e4-4229-aa47-21538ec77c76",
                    "LayerId": "41ad21e8-09b2-471a-9b54-61717aa3c6dc"
                }
            ]
        },
        {
            "id": "01bbf8ca-3525-496a-af9a-b96d9662ebb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972a3335-4b46-4fcc-9bdf-044ca9b45534",
            "compositeImage": {
                "id": "67c02026-3423-46c4-8d77-d551f922ba59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01bbf8ca-3525-496a-af9a-b96d9662ebb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2321d9c-6e62-4782-b042-bfa5aed087ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01bbf8ca-3525-496a-af9a-b96d9662ebb4",
                    "LayerId": "41ad21e8-09b2-471a-9b54-61717aa3c6dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "41ad21e8-09b2-471a-9b54-61717aa3c6dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "972a3335-4b46-4fcc-9bdf-044ca9b45534",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}