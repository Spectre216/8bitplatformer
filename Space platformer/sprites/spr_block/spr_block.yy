{
    "id": "23a90dd7-1d53-4923-91fd-e8b8f18b886c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8779a684-b207-4fc7-8581-e1ca263a831d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23a90dd7-1d53-4923-91fd-e8b8f18b886c",
            "compositeImage": {
                "id": "1cc999f6-8c1c-450a-9a52-622160ca24eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8779a684-b207-4fc7-8581-e1ca263a831d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "253af2b8-038d-4d12-87e2-5004e9e44bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8779a684-b207-4fc7-8581-e1ca263a831d",
                    "LayerId": "eb2e7c03-95e2-447f-955b-83fca991b6c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "eb2e7c03-95e2-447f-955b-83fca991b6c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23a90dd7-1d53-4923-91fd-e8b8f18b886c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}