{
    "id": "f68fc577-bf3e-4a6a-a809-a8198bfa47b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pc_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 2,
    "bbox_right": 5,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45c10c6a-8320-4a09-97d9-1a6ba5d2c6aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f68fc577-bf3e-4a6a-a809-a8198bfa47b2",
            "compositeImage": {
                "id": "aa436c6e-c429-4fc2-90d6-9617d9f4da7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45c10c6a-8320-4a09-97d9-1a6ba5d2c6aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82f3a6e1-ede5-4d40-942d-9ed3b4b7a405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45c10c6a-8320-4a09-97d9-1a6ba5d2c6aa",
                    "LayerId": "9ac8e3b7-d087-4e88-b44d-7ededa44fedf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "9ac8e3b7-d087-4e88-b44d-7ededa44fedf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f68fc577-bf3e-4a6a-a809-a8198bfa47b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}