{
    "id": "241c1a7c-7aa0-4da7-b970-0b6b17cac3c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slime_moving",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 2,
    "bbox_right": 6,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4159d716-1eca-4733-90de-3934f2a87d6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241c1a7c-7aa0-4da7-b970-0b6b17cac3c2",
            "compositeImage": {
                "id": "b50c32e8-96c3-45f5-a609-8882e27efcc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4159d716-1eca-4733-90de-3934f2a87d6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4344c0-5bed-4cf2-a2a6-c6a6c851ee47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4159d716-1eca-4733-90de-3934f2a87d6d",
                    "LayerId": "6a7f4051-a16d-4a7d-9755-f0e6e3f2ca74"
                }
            ]
        },
        {
            "id": "77ded28a-97cb-4fd0-aed6-0c4e0d0dad47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241c1a7c-7aa0-4da7-b970-0b6b17cac3c2",
            "compositeImage": {
                "id": "2ad59d79-c6e7-45ae-aa06-cc02343ac268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ded28a-97cb-4fd0-aed6-0c4e0d0dad47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4da8d88a-41d6-48eb-9431-096095aa1224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ded28a-97cb-4fd0-aed6-0c4e0d0dad47",
                    "LayerId": "6a7f4051-a16d-4a7d-9755-f0e6e3f2ca74"
                }
            ]
        },
        {
            "id": "41f5862c-7cef-4ff4-bab6-cd839381aa9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241c1a7c-7aa0-4da7-b970-0b6b17cac3c2",
            "compositeImage": {
                "id": "8372ecca-4535-4066-85a7-fcd624ac9ecd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41f5862c-7cef-4ff4-bab6-cd839381aa9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93aea3f3-01bf-4373-99a4-7696e7e89285",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41f5862c-7cef-4ff4-bab6-cd839381aa9d",
                    "LayerId": "6a7f4051-a16d-4a7d-9755-f0e6e3f2ca74"
                }
            ]
        },
        {
            "id": "07c05052-e0d1-4db9-bf07-1788041fb8e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "241c1a7c-7aa0-4da7-b970-0b6b17cac3c2",
            "compositeImage": {
                "id": "82bdaba8-d83d-4d7a-bd18-6c9435d48ee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07c05052-e0d1-4db9-bf07-1788041fb8e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91997a8d-c116-4116-a46b-d2cde284834f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07c05052-e0d1-4db9-bf07-1788041fb8e3",
                    "LayerId": "6a7f4051-a16d-4a7d-9755-f0e6e3f2ca74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "6a7f4051-a16d-4a7d-9755-f0e6e3f2ca74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "241c1a7c-7aa0-4da7-b970-0b6b17cac3c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}